<?php

/*
Plugin Name: Ultra Recent Comments & Posts
Plugin URI: http://www.eracer.de
Description: Providing highly customizabel Widgets for Recent Comments and Posts
Version: 1.0.0
Author: Stefan Helmer
Author URI: https://plus.google.com/114798506662127678294/posts
License: GPLv2
*/

define('URC_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('URC_PLUGIN_URL', plugin_dir_url(__FILE__));
define('URC_PLUGIN_RELATIVE_DIR', dirname(plugin_basename(__FILE__)));
define('URC_PLUGIN_NAMESPACE', 'Rockschtar\UltraRecentCommentsAndPosts');

spl_autoload_register(function($class_name) {
    if ( false !== strpos( $class_name, URC_PLUGIN_NAMESPACE) ) {
        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
        $class_name = str_replace(URC_PLUGIN_NAMESPACE . '\\', '', $class_name);
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        /** @noinspection PhpIncludeInspection */
        require_once $classes_dir . $class_file;
    }
});

new Rockschtar\UltraRecentCommentsAndPosts\Controller\PluginController();
