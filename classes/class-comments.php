<?php

class BRC_Comments {

    protected static $max_comment_ID = false;

    public static function get($instance, $max_comment_ID = false) {
        self::$max_comment_ID = $max_comment_ID === null || !$max_comment_ID ? 0 : intval($max_comment_ID);

        $comments_args = array(
            "number" => $instance["number"],
            "status" => $instance["status"],
            "post_type" => $instance["posttypes"],
            "orderby" => $instance["orderby"],
            "order" => $instance["order"],
            "type" => $instance["type"]);

        if (!empty($instance["user_id"]) && $instance["user_id"] != "-1"):
            $comments_args["user_id"] = $instance["user_id"];
        endif;

        if (!empty($instance["author_email"])):
            $comments_args["author_email"] = $instance["author_email"];
        endif;

        if (!empty($instance["search"])):
            $comments_args["search"] = $instance["search"];
        endif;

        $output_format = $instance["output_format"];

        add_filter("comments_clauses", array(__CLASS__, "exclude_comments"), 10, 2);
        remove_filter('get_avatar', 'grofiles_get_avatar');

        $comments = get_comments($comments_args);

        $brc_comments = array();


        foreach ($comments as $comment_test):
            global $comment;
            $comment = $comment_test;

            $comment_ID = $comment->comment_ID;
            $comment_post_ID = $comment->comment_post_ID;

            $comment_replacement_args = array(
                "comment_ID" => $comment_ID,
                "comment_post_ID" => $comment->comment_post_ID,
                "comment_author" => get_comment_author($comment_ID),
                "comment_author_url" => get_comment_author_url($comment_ID),
                "comment_date" => get_comment_date("", $comment_ID),
                "comment_content" => get_comment_text($comment_ID),
                "comment_excerpt" => get_comment_excerpt($comment_ID),
                "comment_link" => esc_url(get_comment_link($comment_ID)),
                "comment_approved" => $comment->comment_approved,
                "comment_karma" => $comment->comment_karma,
                "comment_type" => $comment->comment_type,
                "post_title" => get_the_title($comment_post_ID),
                "post_url" => get_permalink($comment_post_ID),
                "comment_title" => get_the_title($comment_post_ID),
                "avatar_img_tag" => get_avatar(get_comment_author_email($comment_ID), 48)
            );

            $brc_comment_parsed = $output_format;


            foreach ($comment_replacement_args as $key => $value):
                $brc_comment_parsed = str_replace("%" . $key . "%", $value, $brc_comment_parsed);
            endforeach;

            array_push($brc_comments, array("comment_ID" => $comment_ID, "the_comment" => $brc_comment_parsed));

        endforeach;

        return $brc_comments;
    }

    public static function exclude_comments($q, $wpqc) {
        if (self::$max_comment_ID):

            //$ids = implode(', ', self::$comment_IDs_to_exclude);
            $_where_in = " AND comment_ID > " . self::$max_comment_ID;

            if (FALSE !== strpos($q['where'], ' AND comment_ID =')) {
                $q['where'] = preg_replace(
                        '~ AND comment_ID = \d+~', $_where_in, $q['where']
                );
            } else {
                $q['where'] .= $_where_in;
            }
        endif;

        if (isset($wpqc->query_vars['post_type'][0])) {

            global $wpdb;
            $join = join("', '", array_map('esc_sql', $wpqc->query_vars['post_type']));

            $from = "$wpdb->posts.post_type = '" . $wpqc->query_vars['post_type'][0] . "'";
            $to = sprintf("$wpdb->posts.post_type IN ( '%s' ) ", $join);

            $q['where'] = str_replace($from, $to, $q['where']);
        }


        remove_filter('comments_clauses', array(__CLASS__, 'exclude_comments'));
        return $q;
    }

}
