var brc_comment_widget_live = function(widget_id) {
    var that = this;

    this.widget_id = widget_id ? widget_id : null;

    var CommentModel = Backbone.Model.extend({
        default: {comment_ID: 0, the_comment: "", prepend: false}
    });

    var CommentView = Backbone.View.extend({
        tagName: "li",
        template: null,
        events: {},
        initialize: function() {
            _.bindAll(this);
            this.template = _.template('<%= the_comment %>');

        },
        render: function() {
            jQuery(this.el).attr("id", "brc-comment-" + widget_id + "-" + this.model.toJSON().comment_ID);
            jQuery(this.el).html(this.template(this.model.toJSON())).fadeIn();
            return this;
        }
    });

    var CommentCollection = Backbone.Collection.extend({
        model: CommentModel,
        url: brc_comments_widgets.ajaxurl + "?action=brc_get_widget_comments&max_comment_id=0&instance_id=" + that.widget_id + "&nonce=" + brc_comments_widgets.ajaxnonce,
        comparator: function(item) {
            return item.get("comment_ID");
        }
    });

    var CommentsView = Backbone.View.extend({
        tagName: "ul",
        events: {},
        initialize: function() {
            _.bindAll(this);
            this.collection.bind('add', this.addItemHandler);
            //this.collection.bind("sync", this.addItemHandler);
            this.collection.bind("remove", this.removeItemHandler);

        },
        load: function() {
            this.collection.fetch({
                add: true,
                success: this.loadCompleteHandler,
                error: this.errorHandler
            });
        },
        addItemHandler: function(model, options) {
            var comment_view = new CommentView({model: model});
            comment_view.render();

            if (true === model.get("prepend")) {
                jQuery(this.el).prepend(comment_view.el);
            } else {

                jQuery(this.el).append(comment_view.el);
            }
        },
        removeItemHandler: function(model) {
            var comment_view = new CommentView({model: model});
            comment_view.render();
            jQuery("#brc-comment-" + widget_id + "-" + model.get("comment_ID")).remove();
        },
        loadCompleteHandler: function() {
            this.render();
            this.refresh();
        },
        errorHandler: function() {
            throw "Error loading Comments for Widget with ID " + that.widget_id;
        },
        render: function() {
            var container = jQuery('#' + that.widget_id);
            container.append(jQuery(this.el));
            return this;
        },
        refresh: function() {

            var refresh = this;

            refresh.max_comment_id = that.comment_collection.last().get("comment_ID")

            var refresh_interval = setInterval(function() {
                jQuery.ajax({
                    type: "GET",
                    url: brc_comments_widgets.ajaxurl,
                    data: {
                        action: "brc_get_widget_comments",
                        nonce: brc_comments_widgets.ajaxnonce,
                        max_comment_id: refresh.max_comment_id,
                        instance_id: that.widget_id}
                }).done(function(response) {
                    var comments = jQuery.parseJSON(response);

                    for (var key in comments) {             
                        var current_comment = comments[key];
                        that.comment_collection.remove(that.comment_collection.first());
                        that.comment_collection.add(new CommentModel({prepend: true, comment_ID: current_comment.comment_ID, the_comment: current_comment.the_comment}));
                        refresh.max_comment_id = current_comment.comment_ID;
                    }

                    that.comment_collection.sort();

                });
            }, 5000);
        }
    });

    that.comment_collection = new CommentCollection();
    that.comments_view = new CommentsView({collection: that.comment_collection});
    that.comments_view.load();
}


