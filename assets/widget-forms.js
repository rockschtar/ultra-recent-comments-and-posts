jQuery(document).ready(function($) {

    var tooltip_format_cache = "";

    var brcAdvancedOptionsTrigger = function(show, that) {
        var widget_id = $(that).data("widgetid");
        var container_id = "#" + widget_id + "_advanced_options_container";
        var href_show = "#" + widget_id + "_advanced_options_trigger_show";
        var href_hide = "#" + widget_id + "_advanced_options_trigger_hide";

        brcContainerHideAndShow(show, href_show, href_hide, container_id);
    };

    var brcExpertOptionsTrigger = function(show, that) {
        var widget_id = $(that).data("widgetid");
        var container_id = "#" + widget_id + "_expert_options_container";
        var href_show = "#" + widget_id + "_expert_options_trigger_show";
        var href_hide = "#" + widget_id + "_expert_options_trigger_hide";
        brcContainerHideAndShow(show, href_show, href_hide, container_id);
    };

    var brcExpertOptionsHelpTrigger = function (show, that) {
        var widget_id = $(that).data("widgetid");
        var container_id = "#" + widget_id + "_expert_options_help_container";
        var href_show = "#" + widget_id + "_expert_options_help_trigger_show";
        var href_hide = "#" + widget_id + "_expert_options_help_trigger_hide";
        brcContainerHideAndShow(show, href_show, href_hide, container_id);
    };


    var brcContainerHideAndShow = function(show, href_show, href_hide, container_id) {
        if (true === show) {
            $(container_id).slideDown("slow", function() {
                $(href_show).hide();
                $(href_hide).show();
            });
        } else if (false === show) {
            $(container_id).slideUp("slow", function() {
                $(href_show).show();
                $(href_hide).hide();
            });
        }
    };

    var brcShowMoreLinkTrigger = function(that) {
        var widget_id = $(that).data("widgetid");

        if ($(that).prop("checked")) {
            $("#widget-" + widget_id + "-show_more_link_text_container").show();
            $("#widget-" + widget_id + "-show_more_link_url_container").show();
        } else {
            $("#widget-" + widget_id + "-show_more_link_text_container").hide();
            $("#widget-" + widget_id + "-show_more_link_url_container").hide();
        }
    };

    var brcBindEvents = function() {
        $(".brc-expert-options-help-trigger-show").on('click', function () {
            brcExpertOptionsHelpTrigger(true, this);
        });

        $(".brc-expert-options-help-trigger-hide").on('click', function () {
            brcExpertOptionsHelpTrigger(false, this);
        });

        $(".brc-advanced-options-trigger-show").on("click", function() {
            brcAdvancedOptionsTrigger(true, this);
        });

        $(".brc-advanced-options-trigger-hide").on("click", function() {
            brcAdvancedOptionsTrigger(false, this);
        });

        $(".brc-expert-options-trigger-show").on("click", function() {
            brcExpertOptionsTrigger(true, this);
        });

        $(".brc-expert-options-trigger-hide").on("click", function() {
            brcExpertOptionsTrigger(false, this);
        });

        $(".brc-show-more-link-trigger").on("change", function() {

            brcShowMoreLinkTrigger(this);

        });


        $("a").tooltip({
            items: ".brc-tooltip-format",
            open: function(event, ui) {

                $(this).click(function() {

                    $('.ui-tooltip').fadeIn();
                });
            },
            content: function(callback) {

                return callback($("#brc-tooltip-format-content").html());

                if (tooltip_format_cache === "") {
                    $.ajax({
                        url: ajaxurl,
                        dataType: "html",
                        data: {action: "brc_get_output_format_help"},
                        success: function(data) {
                            tooltip_format_cache = data;
                            callback(data)
                        }
                    });
                } else {
                    return tooltip_format_cache;
                }
            }
        });


    }

    jQuery(document).ajaxSuccess(function(e, xhr, settings) {
        if (settings.data && settings.data.search('action=save-widget') !== -1 && (settings.data.search("brc_post_widget") !== -1 || settings.data.search('id_base=brc_comments_widget_static') !== -1 || settings.data.search('id_base=brc_comments_widget_live') !== -1)) {
            brcBindEvents();
        }
    });


    $(".brc-show-more-link-trigger").each(function() {
        brcShowMoreLinkTrigger(this);
    });

    brcBindEvents();
});