var brc_comment_widget_live = function(widget_id, comments_base) {

    var base = this;
    base.widget_id = widget_id;

    var CommentModel = function(comment_ID, the_comment, visible) {
        var that = this;
        that.comment_ID = ko.observable(comment_ID);
        that.the_comment = ko.observable(the_comment);
        that.visible = visible;
        that.element_ID = ko.observable("brc-comment-" + base.widget_id + "-" + comment_ID);
    };

    var WidgetView = function() {
        var that = this;

        var comments = new Array();

        for (var index in comments_base) {
            var comment_base = comments_base[index];
            var comment = new CommentModel(comment_base.comment_ID, comment_base.the_comment, true);
            comments.push(comment);
        }

        that.comments = ko.observableArray(comments);

        that.AddComment = function(comment) {
            var comment_new = new CommentModel(comment.comment_ID, comment.the_comment, false);
            that.comments.unshift(comment_new);
            jQuery("#" + comment_new.element_ID()).fadeIn();
        }

        that.RemoveComment = function(comment) {
            jQuery("#" + comment.element_ID()).fadeOut("slow", function() {
                that.comments.remove(comment);
            });
        }

        that.Refresh = function() {
            var refresh = this;
            var locked = false;
            var first_comment = that.comments()[0];

            refresh.max_comment_id = first_comment.comment_ID();
            
            var refresh_interval = setInterval(function() {
                if (locked === false) {
                    locked = true;
                    jQuery.ajax({
                        type: "GET",
                        url: brc_comments_widgets.ajaxurl,
                        data: {
                            action: "brc_get_widget_comments",
                            nonce: brc_comments_widgets.ajaxnonce,
                            max_comment_id: refresh.max_comment_id,
                            instance_id: base.widget_id}
                    }).done(function(response) {
                        var comments = jQuery.parseJSON(response);
                        for (var key in comments) {
                            var current_comment = comments[key];
                            that.AddComment(current_comment);
                            that.RemoveComment(that.comments()[that.comments().length - 1]);
                            refresh.max_comment_id = that.comments()[0].comment_ID();
                        }
                        locked = false;
                    });
                }

            }, 60000);
        };

        this.Refresh();
    }

    var widgetView = new WidgetView();
    ko.applyBindings(widgetView, document.getElementById(widget_id));
}