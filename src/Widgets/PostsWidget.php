<?php
namespace Rockschtar\UltraRecentCommentsAndPosts\Widgets;

class PostsWidget extends \WP_Widget {

    public function __construct() {
        parent::__construct(
            'brc_post_widget', __('Ultra Recent Posts', 'better_recent_comments'), // Name
            array('description' => __('Bla Bla', 'better_recent_comments'),) // Args
        );
    }

    public function form($instance) {

        $post_types_builtin = get_post_types(array('public' => true, '_builtin' => true), 'objects');
        $post_types_custom = get_post_types(array('public' => true, '_builtin' => false), 'objects');
        $post_types = array_merge($post_types_builtin, $post_types_custom);
        $post_statuses = array(
            'publish' => __('Published', 'better_recent_comments'),
            'future' => __('Future', 'better_recent_comments'),
            'draft' => __('Draft', 'better_recent_comments'),
            'trash' => __('Trash', 'better_recent_comments'),
            'pending' => __('Pending', 'better_recent_comments'),
            'private' => __('Private', 'better_recent_comments'));
        $post_orderbys = array(
            'date' => __('Date', 'better_recent_comments'),
            'modified' => __('Modified Date', 'better_recent_comments'),
            'ID' => __('ID', 'better_recent_comments'),
            'comment_count' => __('Comment Count', 'better_recent_comments'),
            'rand' => __('Random', 'better_recent_comments'),
            'title' => __('Title', 'better_recent_comments'),
            'name' => __('Slug', 'better_recent_comments'));
        $post_order = array('ASC' => __('Ascending', 'better_recent_comments'), 'DESC' => __('Descending', 'better_recent_comments'));

        $instance_title = isset($instance['title']) ? $instance['title'] : __('Recent Posts', 'better_recent_comments');
        $instance_number = isset($instance['number']) ? $instance['number'] : 5;
        $instance_post_types = isset($instance['posttypes']) ? $instance['posttypes'] : array('post');
        $instance_status = isset($instance['status']) ? $instance['status'] : 'publish';
        $instance_categories = isset($instance['categories']) ? (array)$instance['categories'] : array('0');
        $instance_tags = isset($instance['tags']) ? (array)$instance['tags'] : array('0');
        $instance_show_more_link = isset($instance['show_more_link']) ? $instance['show_more_link'] === '1' : false;
        $instance_show_more_link_text = isset($instance['show_more_link_text']) ? $instance['show_more_link_text'] : __('More', 'better_recent_comments');
        $instance_show_more_link_url = isset($instance['show_more_link_url']) ? $instance['show_more_link_url'] : '';

        $instance_user_id = isset($instance['user_id']) ? $instance['user_id'] : '-1';
        $instance_search = isset($instance['search']) ? $instance['search'] : '';
        $instance_orderby = isset($instance['orderby']) ? $instance['orderby'] : 'date';
        $instance_order = isset($instance['order']) ? $instance['order'] : 'DESC';
        $instance_output_format = isset($instance['output_format']) && !empty($instance['output_format']) ? $instance['output_format'] : '<a href="%post_url%" title="%post_title%">%post_title%</a><br />%post_excerpt%<br >%post_date% | %comments_number%';
        ?>
        <p><strong><?php _e('Basic Options', 'better_recent_comments'); ?></strong></p>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'better_recent_comments'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance_title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of comments to show:', 'better_recent_comments'); ?></label>
            <input size="3" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($instance_number); ?>"/>
        </p>
        <p>
            <label><?php _e('Include Posts from these Post-Types:', 'better_recent_comments'); ?></label><br/>
            <?php foreach ($post_types as $post_type): ?>
                <?php $checked = checked(is_array($instance_post_types) && in_array($post_type->name, $instance_post_types), true, false); ?>

                <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('posttype'); ?>-<?php echo $post_type->name; ?>" <?php echo $checked; ?>
                       value="<?php echo $post_type->name; ?>" name="<?php echo $this->get_field_name('posttypes'); ?>[]">
                <label for="<?php echo $this->get_field_name('posttypes'); ?>"><?php echo $post_type->labels->name; ?></label><br>
            <?php endforeach; ?>
        </p>

        <p>
            <a class="brc-advanced-options-trigger-show" href="#brcaction" id="<?php echo $this->id; ?>_advanced_options_trigger_show" data-widgetid="<?php echo $this->id; ?>">Show Advanced
                Options</a>
            <a class="brc-advanced-options-trigger-hide" href="#brcaction" id="<?php echo $this->id; ?>_advanced_options_trigger_hide" data-widgetid="<?php echo $this->id; ?>" style="display: none;">Hide
                Advanced Options</a>
        </p>


        <div style="display:none;" id="<?php echo $this->id ?>_advanced_options_container">
            <p><strong><?php _e('Advanced Options', 'better_recent_comments'); ?></strong></p>
            <p>
                <label for="<?php echo $this->get_field_id('categories'); ?>"><?php _e('Display posts by Category:', 'better_recent_comments'); ?></label>
                <select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>[]" class="widefat" multiple="multiple">
                    <?php $categories = get_categories(array('hide_empty' => false)); ?>
                    <?php foreach ($categories as $category): ?>
                        <option value="<?php echo $category->term_id ?>" <?php selected(in_array($category->term_id, (array)$instance_categories)); ?>><?php echo $category->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('tags'); ?>"><?php _e('Display posts by Tag:', 'better_recent_comments'); ?></label>
                <select id="<?php echo $this->get_field_id('tags'); ?>" name="<?php echo $this->get_field_name('tags'); ?>[]" class="widefat" multiple="multiple">
                    <?php $tags = get_tags(array('hide_empty' => false)); ?>
                    <?php foreach ($tags as $tag): ?>
                        <option value="<?php echo $tag->term_id ?>" <?php selected(in_array($tag->term_id, (array)$instance_tags)); ?>><?php echo $tag->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('status'); ?>"><?php _e('Status:', 'better_recent_comments'); ?></label>
                <select id="<?php echo $this->get_field_id('status'); ?>" name="<?php echo $this->get_field_name('status'); ?>" class="widefat">
                    <?php while ($post_status = current($post_statuses)): ?>
                        <option value="<?php echo key($post_statuses); ?>" <?php selected($instance_status, key($post_statuses)); ?>><?php echo $post_status; ?></option>
                        <?php next($post_statuses); ?>
                    <?php endwhile; ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order By:', 'better_recent_comments'); ?></label>
                <br/><select style="width: 133px;" id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>">
                    <?php while ($post_orderby = current($post_orderbys)): ?>
                        <option value="<?php echo key($post_orderbys); ?>" <?php selected($instance_orderby, key($post_orderbys)); ?>><?php echo $post_orderby; ?></option>
                        <?php next($post_orderbys); ?>
                    <?php endwhile; ?>
                </select>
                <select id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
                    <?php while ($current_post_order = current($post_order)): ?>
                        <option value="<?php echo key($post_order); ?>" <?php selected($instance_order, key($post_order)); ?>><?php echo $current_post_order; ?></option>
                        <?php next($post_order); ?>
                    <?php endwhile; ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('user_id'); ?>"><?php _e('Show post based on certain Author:', 'better_recent_comments'); ?></label>
                <?php wp_dropdown_users(array('name' => $this->get_field_name('user_id'), 'id' => $this->get_field_id('user_id'), 'selected' => $instance_user_id, 'show_option_none' => __('No Restriction', 'better_recent_comments'), 'class' => 'widefat')); ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('search'); ?>"><?php _e('Show posts based on a possible keyword:', 'better_recent_comments'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('search'); ?>" name="<?php echo $this->get_field_name('search'); ?>" type="text"
                       value="<?php echo esc_attr($instance_search); ?>"/>
            </p>
        </div>

        <p>
            <a class="brc-expert-options-trigger-show" href="#brcaction" id="<?php echo $this->id; ?>_expert_options_trigger_show" data-widgetid="<?php echo $this->id; ?>">Show Expert Options</a>
            <a class="brc-expert-options-trigger-hide" href="#brcaction" id="<?php echo $this->id; ?>_expert_options_trigger_hide" data-widgetid="<?php echo $this->id; ?>" style="display: none;">Hide
                Expert Options</a>
        </p>

        <div style="display:none;" id="<?php echo $this->id ?>_expert_options_container">

            <p>
                <label for="<?php echo $this->get_field_id('output_format'); ?>"><?php _e('Output format:', 'better_recent_comments'); ?>
                    &nbsp;
                    <a class="brc-expert-options-help-trigger-show" href="javascript:void(0)"
                       id="<?php echo $this->id; ?>_expert_options_help_trigger_show"
                       data-widgetid="<?php echo $this->id; ?>"><?php _e('Show Help', 'ultra-recent-comments'); ?></a>
                    <a class="brc-expert-options-help-trigger-hide" href="javascript:void(0)"
                       id="<?php echo $this->id; ?>_expert_options_help_trigger_hide"
                       data-widgetid="<?php echo $this->id; ?>"
                       style="display: none;"><?php _e('Hide Help', 'ultra-recent-comments'); ?></a>
                    <aside style="display: none;" id="<?php echo $this->id ?>_expert_options_help_container">
                        <strong><?php _e('Replacement variables:', 'ultra-recents-comments'); ?></strong>
                        <table border="0">
                            <tr>
                                <td>
                                    <pre>%post_ID%</pre>
                                </td>
                                <td><?php _e('The post ID', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_title%</pre>
                                </td>
                                <td><?php _e('The post title', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_content%</pre>
                                </td>
                                <td><?php _e('The post content', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_excerpt%</pre>
                                </td>
                                <td><?php _e('The post excerpt', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_url%</pre>
                                </td>
                                <td><?php _e('The url of the post', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_date%</pre>
                                </td>
                                <td><?php _e('The post date', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%comments_link%</pre>
                                </td>
                                <td><?php _e('The comments link/url', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%comments_number%</pre>
                                </td>
                                <td><?php _e('The comments count', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%comments_number_text%</pre>
                                </td>
                                <td><?php _e('The comments count with text', 'ultra-recent-comments'); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <pre>%post_thumbnail_img%</pre>
                                </td>
                                <td><?php _e('The post thumbnail', 'ultra-recent-comments'); ?></td>
                            </tr>
                        </table>
                    </aside>

                    <textarea class="widefat brc-edit-area" style="height: 80px;"
                              id="<?php echo $this->get_field_id('output_format'); ?>"
                              name="<?php echo $this->get_field_name('output_format'); ?>"><?php echo esc_attr($instance_output_format); ?></textarea>

            </p>
            <p>
                <label for="<?php echo $this->get_field_id('show_more_link'); ?>"><?php _e('Show more link:', 'better_recent_comments'); ?></label>
                <input data-widgetid="<?php echo $this->id; ?>" class="brc-show-more-link-trigger" class="widefat" id="<?php echo $this->get_field_id('show_more_link'); ?>"
                       name="<?php echo $this->get_field_name('show_more_link'); ?>" type="checkbox" value="1" <?php checked($instance_show_more_link, true); ?> />
            </p>
            <p id="<?php echo $this->get_field_id('show_more_link_text'); ?>_container">
                <label for="<?php echo $this->get_field_id('show_more_link_text'); ?>"><?php _e('Show more link text:', 'better_recent_comments'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('show_more_link_text'); ?>" name="<?php echo $this->get_field_name('show_more_link_text'); ?>" type="text"
                       value="<?php echo esc_attr($instance_show_more_link_text); ?>"/>
            </p>
            <p id="<?php echo $this->get_field_id('show_more_link_url'); ?>_container">
                <label for="<?php echo $this->get_field_id('show_more_link_url'); ?>"><?php _e('Show more link URL:', 'better_recent_comments'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('show_more_link_url'); ?>" name="<?php echo $this->get_field_name('show_more_link_url'); ?>" type="text"
                       value="<?php echo esc_attr($instance_show_more_link_url); ?>"/>
            </p>
        </div>


        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();

        $instance['title'] = !empty($new_instance['title']) ? filter_var($new_instance['title'], FILTER_SANITIZE_STRING) : '';
        $instance['number'] = !empty($new_instance['number']) ? filter_var($new_instance['number'], FILTER_SANITIZE_NUMBER_INT) : 5;
        $instance['posttypes'] = (!is_array($new_instance['posttypes'])) ? array() : $new_instance['posttypes'];
        $instance['categories'] = (!is_array($new_instance['categories'])) ? array() : $new_instance['categories'];
        $instance['show_more_link'] = !empty($new_instance['show_more_link']) ? filter_var($new_instance['show_more_link'], FILTER_SANITIZE_NUMBER_INT) : 0;
        $instance['show_more_link_text'] = !empty($new_instance['show_more_link_text']) ? filter_var($new_instance['show_more_link_text'], FILTER_SANITIZE_STRING) : '';
        $instance['show_more_link_url'] = !empty($new_instance['show_more_link_url']) ? filter_var($new_instance['show_more_link_url'], FILTER_SANITIZE_URL) : '';
        $instance['user_id'] = (!empty($new_instance['user_id']) && $new_instance['user_id'] !== '-1') ? filter_var($new_instance['user_id'], FILTER_SANITIZE_STRING) : null;
        $instance['search'] = !empty($new_instance['search']) ? filter_var($new_instance['search'], FILTER_SANITIZE_STRING) : '';
        $instance['orderby'] = !empty($new_instance['orderby']) ? filter_var($new_instance['orderby'], FILTER_SANITIZE_STRING) : 'date';
        $instance['order'] = !empty($new_instance['order']) ? filter_var($new_instance['order'], FILTER_SANITIZE_STRING) : 'DESC';
        $instance['title'] = !empty($new_instance['title']) ? filter_var($new_instance['title'], FILTER_SANITIZE_STRING) : '';
        $instance['output_format'] = !empty($new_instance['output_format']) ? $new_instance['output_format'] : '';

        return $instance;
    }

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];

        if (!empty($title)):
            echo $args['before_title'] . $title . $args['after_title'];
        endif;

        $query_args = array(
            'post_type' => $instance['posttypes'],
            'orderby' => $instance['orderby'],
            'order' => $instance['order'],
            'posts_per_page' => $instance['number'],
            'cache_results' => true,
        );

        if (array_key_exists('user_id', $instance)):
            $query_args['author'] = $instance['user_id'];
        endif;


        if (array_key_exists('search', $instance) && !empty($instance['search'])):
            $query_args['s'] = $instance['search'];
        endif;

        if ($instance['categories']):
            $query_args['cat'] = $instance['categories'];
        endif;

        $show_more_link = $instance['show_more_link'] === '1';
        $output_format = $instance['output_format'];
        $custom_posts = get_posts($query_args);
        ?>
        <ul>
            <?php
            if ($custom_posts):
                foreach ($custom_posts as $custom_post):

                    setup_postdata($GLOBALS['post'] = &$custom_post);
                    $comments_number = (int)get_comments_number();

                    if ($comments_number > 1):
                        $comments_number_text = sprintf(_n('%s Comment', '%s Comments', $comments_number), number_format_i18n($comments_number));
                    elseif ($comments_number === 0):
                        $comments_number_text = __('No Comments');
                    else:
                        $comments_number_text = __('1 Comment');
                    endif;

                    $post_replacement_args = array(
                        'post_ID' => get_the_ID(),
                        'post_title' => get_the_title(),
                        'post_content' => get_the_content(),
                        'post_excerpt' => get_the_excerpt(),
                        'post_url' => get_permalink(),
                        'post_date' => get_the_date(),
                        'comments_link' => get_comments_link(),
                        'comments_number' => $comments_number,
                        'comments_number_text' => $comments_number_text,
                        'post_thumbnail_img' => get_the_post_thumbnail(get_the_ID(), array(96, 96))
                    );

                    $brc_post_parsed = $output_format;

                    foreach ($post_replacement_args as $key => $value):
                        $brc_post_parsed = str_replace('%' . $key . '%', $value, $brc_post_parsed);
                    endforeach;

                    echo "<li>{$brc_post_parsed}</li>";

                endforeach;

                wp_reset_postdata();

            else:
                ?>
                <li><?php _e('No recent posts found', 'ultra-recent-comments'); ?></li><?php
            endif;

            if ($show_more_link):
                ?>
                <li><a href="<?php echo $instance['show_more_link_url'] ?>"><?php echo $instance['show_more_link_text'] ?></a></li><?php
            endif;
            ?></ul>
        <?php
        echo $args['after_widget'];
    }

}
