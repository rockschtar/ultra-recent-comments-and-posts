<?php

namespace Rockschtar\UltraRecentCommentsAndPosts\Widgets;


use Rockschtar\UltraRecentCommentsAndPosts\Classes\Comments;

class CommentsStaticWidget extends CommentsBaseWidget {
    public function __construct() {
        parent::__construct(
            'urc_comments_widget_static', __('Ultras Recent Comments', 'better_recent_comments'), // Name
            array('description' => __('A Foo Widget', 'better_recent_comments'),) // Args
        );
    }

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];

        if (!empty($title)):
            echo $args['before_title'] . $title . $args['after_title'];
        endif;

        $comments_args = array(
            "number" => $instance["number"],
            "status" => $instance["status"],
            "post_type" => $instance["posttypes"],
            "orderby" => $instance["orderby"],
            "order" => $instance["order"],
            "type" => $instance["type"]);

        if (!empty($instance["user_id"]) && $instance["user_id"] != "-1"):
            $comments_args["user_id"] = $instance["user_id"];
        endif;

        if (!empty($instance["author_email"])):
            $comments_args["author_email"] = $instance["author_email"];
        endif;

        if (!empty($instance["search"])):
            $comments_args["search"] = $instance["search"];
        endif;

        $comments = Comments::Get($instance, false);

        ?>
        <ul>
            <?php
            foreach ($comments as $comment):
                /*$comment_ID = $comment->comment_ID;
                $comment_author = $comment->comment_author;
                $comment_link = get_comment_link($comment_ID);
                $comment_excerpt = get_comment_excerpt($comment_ID);
                $comment_title = get_the_title($comment->comment_post_ID) . ", " . get_comment_date(get_option('date_format'), $comment_ID);`*/
                //$comment_display = '<a href="' . $comment_link . '" title="' . $comment_title . '">' . $comment_author . '</a>: ' . $comment_excerpt;
                ?>
                <li><?php echo $comment['the_comment']; ?></li><?php
            endforeach;
            ?>
        </ul>
        <?php
        echo $args['after_widget'];
    }

}
