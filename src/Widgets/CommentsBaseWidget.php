<?php

namespace Rockschtar\UltraRecentCommentsAndPosts\Widgets;

class CommentsBaseWidget extends \WP_Widget {

    public function __construct($id, $title, $args) {
        parent::__construct($id, $title, $args);
    }

    public function form($instance) {

        $post_types_builtin = get_post_types(array('public' => true, '_builtin' => true), 'objects');
        $post_types_custom = get_post_types(array('public' => true, '_builtin' => false), 'objects');
        $post_types = array_merge($post_types_builtin, $post_types_custom);
        $comment_statuses = array('approve' => 'Approved', 'hold' => 'Awaiting Moderation', 'spam' => 'Spam', 'trash' => 'Trash');
        $comment_orderbys = array('comment_date_gmt' => 'Date', 'karma' => 'Karma', 'type' => 'Type');
        $comment_order = array('ASC' => __('Ascending', 'better_recent_comments'), 'DESC' => __('Descending', 'better_recent_comments'));
        $comment_types = array('' => 'Normal (Comments, Pingback & Trackback)', 'comment' => 'Comments only', 'pingback' => 'Pingbacks only', 'trackback' => 'Trackback only', 'pings' => 'Pingback & Trackback only');

        $instance_title = isset($instance['title']) ? $instance['title'] : __('Recent Comments', 'better_recent_comments');
        $instance_number = isset($instance['number']) ? $instance['number'] : 5;
        $instance_post_types = isset($instance['posttypes']) ? $instance['posttypes'] : array('post', 'page');
        $instance_status = isset($instance['status']) ? $instance['status'] : 'approve';
        $instance_type = isset($instance['type']) ? $instance['type'] : '';

        $instance_user_id = isset($instance['user_id']) ? $instance['user_id'] : '-1';
        $instance_author_email = isset($instance['author_email']) ? $instance['author_email'] : '';
        $instance_search = isset($instance['search']) ? $instance['search'] : '';
        $instance_output_format = isset($instance['output_format']) && !empty($instance['output_format']) ? $instance['output_format'] : '<a href="%comment_link%" title="%comment_title%">%comment_author%</a>: %comment_excerpt%';


        if ('brc_comments_widget_static' === $this->id_base):
            $instance_orderby = isset($instance['orderby']) ? $instance['orderby'] : 'comment_date_gmt';
            $instance_order = isset($instance['order']) ? $instance['order'] : 'DESC';
        endif;
        ?>
        <p><strong><?php _e('Basic Options', 'better_recent_comments'); ?></strong></p>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'better_recent_comments'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance_title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of comments to show:', 'better_recent_comments'); ?></label>
            <input size="3" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($instance_number); ?>" />
        </p>
        <p>
            <label><?php _e('Include Comments from these Post-Types:', 'better_recent_comments'); ?></label><br />
            <?php foreach ($post_types as $post_type): ?>
                <?php $checked = checked(is_array($instance_post_types) && in_array($post_type->name, $instance_post_types), true, false); ?>

                <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('posttype'); ?>-<?php echo $post_type->name; ?>" <?php echo $checked; ?> value="<?php echo $post_type->name; ?>" name="<?php echo $this->get_field_name('posttypes'); ?>[]">
                <label for="<?php echo $this->get_field_name('posttypes'); ?>"><?php echo $post_type->labels->name; ?></label><br>
            <?php endforeach; ?>
        </p>

        <p>
            <a class="brc-advanced-options-trigger-show" href="#brcaction" id="<?php echo $this->id; ?>_advanced_options_trigger_show" data-widgetid="<?php echo $this->id; ?>">Show Advanced Options</a>
            <a class="brc-advanced-options-trigger-hide" href="#brcaction" id="<?php echo $this->id; ?>_advanced_options_trigger_hide" data-widgetid="<?php echo $this->id; ?>" style="display: none;">Hide Advanced Options</a>
        </p>
        <div style="display:none;" id="<?php echo $this->id ?>_advanced_options_container">
            <p><strong><?php _e('Advanced Options', 'better_recent_comments'); ?></strong></p>
            <p>
                <label for="<?php echo $this->get_field_id('status'); ?>"><?php _e('Status:', 'better_recent_comments'); ?></label>
                <select id="<?php echo $this->get_field_id('status'); ?>" name="<?php echo $this->get_field_name('status'); ?>" class="widefat">
                    <?php while ($comment_status = current($comment_statuses)): ?>
                        <option value="<?php echo key($comment_statuses); ?>" <?php selected($instance_status, key($comment_statuses)); ?>><?php echo $comment_status; ?></option>
                        <?php next($comment_statuses); ?>
                    <?php endwhile; ?>
                </select>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('type'); ?>"><?php _e('Type:', 'better_recent_comments'); ?></label>
                <select id="<?php echo $this->get_field_id('type'); ?>" name="<?php echo $this->get_field_name('type'); ?>" class="widefat">
                    <?php while ($comment_type = current($comment_types)): ?>
                        <option value="<?php echo key($comment_types); ?>" <?php selected($instance_type, key($comment_types)); ?>><?php echo $comment_type; ?></option>
                        <?php next($comment_types); ?>
                    <?php endwhile; ?>
                </select>
            </p>

            <?php if ('brc_comments_widget_static' === $this->id_base): ?>
                <p>
                    <label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order By:', 'better_recent_comments'); ?></label>
                    <select id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>">
                        <?php while ($comment_orderby = current($comment_orderbys)): ?>
                            <option value="<?php echo key($comment_orderbys); ?>" <?php selected($instance_orderby, key($comment_orderbys)); ?>><?php echo $comment_orderby; ?></option>
                            <?php next($comment_orderbys); ?>
                        <?php endwhile; ?>
                    </select>
                    <select id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
                        <?php while ($current_comment_order = current($comment_order)): ?>
                            <option value="<?php echo key($comment_order); ?>" <?php selected($instance_order, key($comment_order)); ?>><?php echo $current_comment_order; ?></option>
                            <?php next($comment_order); ?>
                        <?php endwhile; ?>
                    </select>
                </p>
            <?php endif; ?>
            <p>
                <label for="<?php echo $this->get_field_id('user_id'); ?>"><?php _e('Show comments based on certain User:', 'better_recent_comments'); ?></label>
                <?php wp_dropdown_users(array('name' => $this->get_field_name('user_id'), 'id' => $this->get_field_id('user_id'), 'selected' => $instance_user_id, 'show_option_none' => __('No Restriction', 'better_recent_comments'), 'class' => 'widefat')); ?>
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('author_email'); ?>"><?php _e('Show comments based on certain author email:', 'better_recent_comments'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('author_email'); ?>" name="<?php echo $this->get_field_name('author_email'); ?>" type="text" value="<?php echo esc_attr($instance_author_email); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('search'); ?>"><?php _e('Show comments based on a possible keyword:', 'better_recent_comments'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('search'); ?>" name="<?php echo $this->get_field_name('search'); ?>" type="text" value="<?php echo esc_attr($instance_search); ?>" />
            </p>
        </div>

        <p>
            <a class="brc-expert-options-trigger-show" href="#brcaction" id="<?php echo $this->id; ?>_expert_options_trigger_show" data-widgetid="<?php echo $this->id; ?>">Show Expert Options</a>
            <a class="brc-expert-options-trigger-hide" href="#brcaction" id="<?php echo $this->id; ?>_expert_options_trigger_hide" data-widgetid="<?php echo $this->id; ?>" style="display: none;">Hide Expert Options</a>
        </p>

        <div style="display:block;" id="<?php echo $this->id ?>_expert_options_container">
            <p>
                <label for="<?php echo $this->get_field_id('output_format'); ?>"><?php _e('Output format:', 'better_recent_comments'); ?>&nbsp;<a class="brc-tooltip-format" href="#brctooltip">Help</a></label>
                <textarea class="widefat" id="<?php echo $this->get_field_id('output_format'); ?>" name="<?php echo $this->get_field_name('output_format'); ?>" type="text"><?php echo esc_attr($instance_output_format); ?></textarea>
            </p>
        </div>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();

        $instance['title'] = (!empty($new_instance['title'])) ? filter_var($new_instance['title'], FILTER_SANITIZE_STRING) : '';
        $instance['number'] = (!empty($new_instance['number'])) ? filter_var($new_instance['number'], FILTER_SANITIZE_NUMBER_INT) : 5;
        $instance['posttypes'] = (!is_array($new_instance['posttypes'])) ? array() : $new_instance['posttypes'];
        $instance['status'] = filter_var($new_instance['status'], FILTER_SANITIZE_STRING);
        $instance['type'] = filter_var($new_instance['type'], FILTER_SANITIZE_STRING);

        $instance['user_id'] = filter_var($new_instance['user_id'], FILTER_SANITIZE_NUMBER_INT);
        $instance['author_email'] = filter_var($new_instance['author_email'], FILTER_SANITIZE_STRING);
        $instance['search'] = trim(filter_var($new_instance['author_email'], FILTER_SANITIZE_STRING));
        $instance['output_format'] = $new_instance['output_format'];

        if ('brc_comments_widget_static' === $this->id_base):
            $instance['orderby'] = filter_var($new_instance['orderby'], FILTER_SANITIZE_STRING);
            $instance['order'] = filter_var($new_instance['order'], FILTER_SANITIZE_STRING);
        else:
            $instance['orderby'] = 'comment_date_gmt';
            $instance['order'] = 'DESC';
            update_option($this->id, $instance);
        endif;
        return $instance;
    }

}
