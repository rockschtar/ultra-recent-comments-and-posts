<?php
namespace Rockschtar\UltraRecentCommentsAndPosts\Controller;

use Rockschtar\UltraRecentCommentsAndPosts\Widgets\CommentsStaticWidget;
use Rockschtar\UltraRecentCommentsAndPosts\Widgets\PostsWidget;

class PluginController {

    public function __construct() {

        add_action('widgets_init', array(&$this, 'widgets_init'));
        add_action('wp_enqueue_scripts', array(&$this, 'enqueue_widget_scripts'));
        add_action('admin_print_scripts-widgets.php', array(&$this, 'admin_print_scripts_widgets'), 20);
        add_action('admin_footer-widgets.php', array(&$this, 'tooltip_format_content'));
        add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
        add_filter('query_vars', array(&$this, 'query_vars'));
        new AjaxController();

    }

    public function plugins_loaded() {
        if (is_admin()):
        // load_plugin_textdomain("better_recent_comments", false, BRC_PLUGIN_RELATIVE_DIR . '/language');
        endif;
    }

    public function query_vars($query_vars) {
        $query_vars[] = 'brc_query';
        return $query_vars;
    }

    public function enqueue_widget_scripts() {

        wp_register_script('brc-knockout', URC_PLUGIN_URL . 'assets/knockout-3.1.0.js');

        if (is_active_widget(false, false, 'brc_comments_widget_live')):
            wp_enqueue_script('brc-comments-widgets', URC_PLUGIN_URL . 'assets/comment-widget-live.knockout.js', array('jquery', 'jquery-effects-slide', 'brc-knockout'));
            wp_localize_script('brc-comments-widgets', 'brc_comments_widgets', array('ajaxurl' => admin_url('admin-ajax.php'), 'ajaxnonce' => wp_create_nonce('ultra-recent-comments-ajax')));
        endif;
    }

    public function widgets_init() {
        register_widget(CommentsStaticWidget::class);
        register_widget(PostsWidget::class);
    }

    public function admin_print_scripts_widgets() {
        //wp_enqueue_style("brc-jquery-ui-mp6", BRC_PLUGIN_URL . "assets/jquery-ui-1.10.3.mp6.css");
        self::load_jquery_ui();

        wp_register_script('urcp-codemirror', 'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.min.js');

        wp_enqueue_style('urcp-codemirror', 'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.css');

        wp_enqueue_script('brc-widget-forms', URC_PLUGIN_URL . 'assets/widget-forms.js', array('jquery-ui-tooltip', 'urcp-codemirror'));
        wp_localize_script('brc-widget-forms', 'brc_widget_forms', array('pluginurl' => URC_PLUGIN_URL));
        //wp_enqueue_script("brc-widget", BRC_PLUGIN_URL . "assets/widget.js", array("jquery", "jquery-ui-core", "jquery-ui-tabs"));
    }

    public function tooltip_format_content() {
        ?>
        <div id="brc-tooltip-format-content">
            <strong><?php _e('Replacement variables:', 'ultra-recents-comments'); ?></strong>

            <table border="0">
                <tr>
                    <td><pre>%post_ID%</pre></td>
                    <td><?php _e('The post ID', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_title%</pre></td>
                    <td><?php _e('The post title', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_content%</pre></td>
                    <td><?php _e('The post content', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_excerpt%</pre></td>
                    <td><?php _e('The post excerpt', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_url%</pre></td>
                    <td><?php _e('The url of the post', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_date%</pre></td>
                    <td><?php _e('The post date', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%comments_link%</pre></td>
                    <td><?php _e('The comments link/url', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%comments_number%</pre></td>
                    <td><?php _e('The comments count', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%comments_number_text%</pre></td>
                    <td><?php _e('The comments count with text', 'ultra-recent-comments'); ?></td>
                </tr>
                <tr>
                    <td><pre>%post_thumbnail_img%</pre></td>
                    <td><?php _e('The post thumbnail', 'ultra-recent-comments'); ?></td>
                </tr>
            </table>


        </div>
        <?php
    }

    private static function load_jquery_ui() {
        global $wp_scripts;
        $ui = $wp_scripts->query('jquery-ui-core');
        $protocol = is_ssl() ? 'https' : 'http';
        $url = "$protocol://ajax.googleapis.com/ajax/libs/jqueryui/{$ui->ver}/themes/smoothness/jquery-ui.min.css";
        wp_enqueue_style('jquery-ui-smoothness', $url, false, null);
    }

}
