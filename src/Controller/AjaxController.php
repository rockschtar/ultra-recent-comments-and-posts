<?php

namespace Rockschtar\UltraRecentCommentsAndPosts\Controller;

use Rockschtar\UltraRecentCommentsAndPosts\Classes\Comments;

class AjaxController {

    public function __construct() {
        add_action('wp_ajax_brc_get_widget_comments', array(&$this, 'get_widget_comments'));
        add_action('wp_ajax_nopriv_brc_get_widget_comments', array(&$this, 'get_widget_comments'));
    }

    public function get_widget_comments() {

        check_ajax_referer('ultra-recent-comments-ajax', 'nonce');

        $widget_instance_id = filter_input(INPUT_GET, 'instance_id', FILTER_SANITIZE_STRING);
        $instance = get_option($widget_instance_id);

        $exclude = filter_input(INPUT_GET, 'exclude', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $max_comment_id = filter_input(INPUT_GET, 'max_comment_id', FILTER_SANITIZE_NUMBER_INT);

        $comments = Comments::get($instance, $max_comment_id);

        echo json_encode($comments);
        die;
    }

}
